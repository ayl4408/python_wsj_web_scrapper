import libxml2
import sys
import os
import commands
import re
import sys
from xml.dom.minidom import parse, parseString
from cStringIO import StringIO
from xml.parsers import expat
import locale
import mysql.connector
import time

locale.setlocale( locale.LC_ALL, 'en_US.UTF-8' )
cnx = mysql.connector.connect(user='root', password='1000',host='localhost', database='stock')
cursor = cnx.cursor()
tbl=str(time.strftime("%I%M%S_%d%m%Y"))

def syntax_based_parse(l):
   c=[]
   p=[]
   for x in l:
      c = get_text(x)
      if len(c) == 8: #length of the information will be 8
         p.append(c)
   return p

def get_text(x):
   lst=[]
   if x.nodeType in (3,4):
      lst.append((x.nodeValue).replace("'", ""))
   else:
      for y in x.childNodes:
         lst = lst + get_text(y)
   return lst

def organize(p):
   stock={}
   for x in range(1,101): #skip first element because it is a header
      stock[int(p[x][0])]={'name': p[x][2].strip('\n'), 'volume': locale.atoi(p[x][4]), 'price': float(p[x][5].strip('$')), 'chg': float(p[x][6]), 'chg%': float(p[x][7])}
   return stock

#******************************************************#

#                    MAIN FUNCTION                     #

#******************************************************#

def main():
   xhtml_fn = sys.argv[1]
   global dom
   dom = parse(xhtml_fn)
   table_rows=dom.getElementsByTagName('tr')
   stock=organize(syntax_based_parse(table_rows))

   s="create table " + tbl + " (volume int, price float, name varchar(200), chg_percent float, chg float);"
   cursor.execute(s)

   for num in stock:
      volume=stock[num]['volume']
      price=stock[num]['price']
      name=stock[num]['name']
      chg1=stock[num]['chg%']
      chg2=stock[num]['chg']
      w="insert into " + tbl + " values " + "(%d, %.2f, '%s', %.2f, %.2f)" % (volume, float(price), name, float(chg1), float(chg2))+";"
      cursor.execute(w)
      #print(volume)
      #print volume

   cursor.close()
   cnx.commit()
   cnx.close()
         
# end of main()

if __name__ == "__main__":
    main()
